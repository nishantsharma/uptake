({
	doInit : function(component, event, helper) { 
       
       helper.initializeData(component, event, helper);
    },
    sortByCaseNumber: function(component, event, helper) {
        
        helper.sortBy(component, "CaseNumber");
    },
    sortBySubject: function(component, event, helper) {
        
        helper.sortBy(component, "Subject");
    },
    sortByProduct: function(component, event, helper) {
        
        helper.sortBy(component, "Canam_Product__c");
    },
    sortByContactName: function(component, event, helper) {
        
        helper.sortBy(component, "ContactName");
    },
    sortByStatus: function(component, event, helper) {
        
        helper.sortBy(component, "Status");
    },
    sortByPriority: function(component, event, helper) {
        
        helper.sortBy(component, "Priority");
    },
    sortByEnvironment: function(component, event, helper) {
        
		helper.sortBy(component, "BERT_Environment__c");        
    },
    sortByCreatedDate: function(component, event, helper) {
        
		helper.sortBy(component, "CreatedDate");        
    },
    sortByLastModifiedDate : function(component, event, helper) {
        
        helper.sortBy(component, "LastModifiedDate");  
    },
    sortByBertNumber : function(component, event, helper) {
        
        helper.sortBy(component, "BERT_Ticket_Number__c");  
    },
    sortByBertQuote : function(component, event, helper) {
        
        helper.sortBy(component, "BERT_Request_for_Quote__c");  
    },
    renderCurrentPage: function(component, event, helper) {
        
        helper.renderPage(component,event, helper);
    },
    filterCaseRelatedData: function(component, event, helper) {
        
        helper.filterCases(component, event, helper);
    },
    resetFilters : function(component, event, helper) {
		
        helper.resetCaseFilters(component);
    }
    
})