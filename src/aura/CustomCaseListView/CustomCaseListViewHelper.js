({
    initializeData : function (component, event, helper){
        
        component.set("v.isLoading",true);
        var action = component.get("c.fetchCaseDetails");
        action.setCallback(this, function(result) {
            
            var selectedStatusValues = component.get("v.selectedStatusValues");
            var records = result.getReturnValue();
            
            for(var index = 0; index < records.lstCases.length; index++) {
                
                if(records.lstCases[index].ContactId) {
                    
                    records.lstCases[index].ContactName = records.lstCases[index].Contact.Name;
                } else {
                    
                    records.lstCases[index].ContactName = "";
                }
            }
            
            component.set("v.fetchedCases", records.lstCases);
            component.set("v.statusValues",records.mapValues['Status']);
            component.find("status").set("v.value", selectedStatusValues);
            component.set("v.priorityValues",records.mapValues['Priority']);
            component.set("v.productValues",records.mapValues['Canam_Product__c']);
            component.set("v.environmentValues",records.mapValues['BERT_Environment__c']);
            component.set("v.maxPage", Math.floor((records.lstCases.length+9)/10));
            this.sortBy(component, "CaseNumber");
            component.set("v.isLoading",false);
        });
        $A.enqueueAction(action);
        
    },
    sortBy: function(component, field) {
        
        console.log('product called in helper');
        var sortAsc = component.get("v.sortAsc"),
            sortField = component.get("v.sortField"),
            records = component.get("v.fetchedCases");
        sortAsc = sortField != field || !sortAsc;
        
        records.sort(function(a,b){
            
            var aVal = a[field];
            var bVal = b[field];
            
            var t1 = aVal == bVal,
                t2 = (!aVal && bVal) || (aVal < bVal);
            return t1? 0: (sortAsc?-1:1)*(t2?1:-1);
        });
        
        component.set("v.sortAsc", sortAsc);
        component.set("v.sortField", field);
        component.set("v.fetchedCases", records);
        this.renderPage(component);
        
    },
    renderPage: function(component,event, helper) {
        
        var records = component.get("v.fetchedCases"),
            pageNumber = component.get("v.pageNumber"),
            pageRecords = records.slice((pageNumber-1)*10, pageNumber*10);
        component.set("v.currentList", pageRecords);
    },
    filterCases: function(component, event, helper) {
        
        component.set("v.isLoading",true);
        var selectedStatus = component.find("status").get("v.value");
        var selectedPriority = component.find("priority").get("v.value");
        var selectedEnvironment = component.find("environment").get("v.value");
        var selectedProduct = component.find("product").get("v.value");
        var isCheckedRFQ = component.get("v.isCheckedRFQ");
        
        var action = component.get("c.filterCaseData");
        
        action.setParams({
            			"statusFilters":selectedStatus,
                        "priorityFilters" :selectedPriority,
                        "environmentFilters":selectedEnvironment,
                        "productFilters":selectedProduct,
                        "isCheckedFilters": isCheckedRFQ
        				});
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                
                var result  = response.getReturnValue();
                
                console.log('## result : ',result);
                
                for(var index = 0; index < result.lstCases.length; index++) {
                    
                    if(result.lstCases[index].ContactId) {
                        
                        result.lstCases[index].ContactName = result.lstCases[index].Contact.Name;
                    } else {
                        
                        result.lstCases[index].ContactName = "";
                    }
                }
                
                component.set("v.fetchedCases", result.lstCases);
                component.set("v.maxPage", Math.floor((result.lstCases.length+9)/10));
                component.set("v.pageNumber", 1);
                this.sortBy(component, "CaseNumber");
                component.set("v.isLoading",false);
                
            }
        });
        
        $A.enqueueAction(action);
    },
    resetCaseFilters : function(component, event, helper) {
        
        component.set("v.isLoading",true);
        
        var action = component.get("c.filterCaseData");
        action.setParams({
            				"statusFilters":"",
            				"priorityFilters" :"",
							"environmentFilters":"",
							"productFilters":"",
							"isCheckedFilters":""
                         });
        
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            
            if (component.isValid() && state === "SUCCESS") {
                
                var result  = response.getReturnValue();
                
                component.set("v.fetchedCases", result.lstCases);
                component.set("v.maxPage", Math.floor((result.lstCases.length+9)/10));
                component.set("v.pageNumber", 1);
                component.find("status").set("v.value","");
                component.find("priority").set("v.value","");
                component.find("environment").set("v.value","");
                component.find("product").set("v.value", "");
                component.set("v.isCheckedRFQ", '');
                this.sortBy(component, "CaseNumber");
                component.set("v.isLoading",false);
                
            }
        });
        
        $A.enqueueAction(action);
    }
})