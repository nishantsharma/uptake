({
	onInit : function(component, event, helper){
		helper.doCallout(component, "c.initializeModel", null).then(function(data){
			//set default values to first option
			data.record.Description = '';
            data.record.Subject = '';
			helper.setDefault(data, 'BERT_Environment__c');
			helper.setDefault(data, 'Priority');
			helper.setDefault(data, 'Canam_Product__c');
			data.record.Internal_Ticket_Number__c = '';
			//set model
			component.set("v.model", data);
            component.get("v.model").record = data.record;
			component.set("v.fileList", new Array());
		})

		helper.renderLookup(component, helper, "contact", function(contact){
			component.get("v.model").record.ContactId = contact.data.Id;
		},
		function(){
			component.get("v.model").record.ContactId = null;
		});

		helper.renderLookup(component, helper, "firstContact", function(contact){
			component.get("v.model").firstContact = contact.data.Id;
		},
		function(){
			component.get("v.model").firstContact = null;
		});

		helper.renderLookup(component, helper, "secondContact", function(contact){
			component.get("v.model").secondContact = contact.data.Id;
		},
		function(){
			component.get("v.model").secondContact = null;
		});
	},

	doSave : function(component, event, helper){
        //Thanks locker service
        component.get("v.model").record.Subject = component.find("in_subject").get("v.value");
        component.get("v.model").record.Description = component.find("in_description").get("v.value");
        component.get("v.model").record.Priority = component.find("in_priority").get("v.value");
        component.get("v.model").record.Internal_Ticket_Number__c = component.find("in_ticketNumber").get("v.value");
        component.get("v.model").record.Canam_Product__c = component.find("in_canam").get("v.value");
        if(component.find("in_env") && component.find("in_env").get("v.value"))
        	component.get("v.model").record.BERT_Environment__c = component.find("in_env").get("v.value");
        component.get("v.model").record.BERT_Request_for_Quote__c = component.find("requestForQuote").get("v.checked");
               
		//Validate form
		if(helper.validate(component)){

			helper.showSpinner(component, event, helper);
			helper.doCallout(component, "c.saveCase", {modelData : JSON.stringify(component.get("v.model"))}).then(function(response){
				if(response.success && response.record){

					var promiseArray = new Array();
					for(var index in component.get("v.fileList")){
						var p = new Promise(function(resolve, reject){
							helper.uploadChunk(component, component.get("v.fileList")[index].fileName, component.get("v.fileList")[index].contentType, component.get("v.fileList")[index].base64Data, 0, helper.CHUNK_SIZE, '', response.record, resolve);
						});
						promiseArray.push(p);
					}
					Promise.all(promiseArray).then(function(callbacks){
						helper.hideSpinner(component, event, helper);
						var navEvt = $A.get("e.force:navigateToSObject");
						navEvt.setParams({
							"recordId": response.record,
							"slideDevName": "detail"
						});
						navEvt.fire();
					})
				}else{
					helper.showToast("Failed to create case", response.message, "error")
				}
			});
		}else{
			helper.showToast("Missing Required Fields", "Some required fields are missing", "error");
		}
	},
	goBack : function(component, event, helper){
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams(({"url" : "/"}));
		urlEvent.fire();
	},
	openFileDialog : function(component, event, helper) {
        var input = $(component.find("fileInput").getElement());
        input.trigger('click'); // opening dialog
        return false; // avoiding navigation
	},
	fileSelected : function(component, event, helper){
		var file = component.find("fileInput").getElement().files[0];
		var size = 0;
		for(var index in component.get("v.fileList")){
			size+= component.get("v.fileList")[index].size;
		}
		size += file.size;

		// if(size >= 950000)
		// 	helper.showToast("File Limit Exceeded", "File uploads cannot exceed a total of 1MB", "error");
		// else{
			helper.readFile(component, helper, file, function(data){
				component.set("v.fileList", component.get("v.fileList").concat(data));
			});
		//}
	},
	removeFile : function(component, event, helper){
		component.get("v.fileList").splice($(event.currentTarget).data("index"), 1);
		component.set("v.fileList", component.get("v.fileList"));
	}
})