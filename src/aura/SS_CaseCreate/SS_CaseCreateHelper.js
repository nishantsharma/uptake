({
	MAX_FILE_SIZE: 4 500 000, /* 6 000 000 * 3/4 to account for base64 */
	CHUNK_SIZE: 500 000, /* Use a multiple of 4 */


	doCallout : function(component, endpoint, params) {
		return new Promise(function(resolve, reject){
			var action = component.get(endpoint);
			if(params)
				action.setParams(params);

			action.setCallback(this, function(response){
				resolve(response.getReturnValue());
			});
			$A.enqueueAction(action);
		})
	},
	setDefault : function(data, option){
		data.record[option] = (!data.record[option] && data.optionMap[option] && data.optionMap[option].length > 0) ? data.optionMap[option][0] : data.record[option];
	},
	renderLookup : function(component, helper, element, onSelect, onInvalidate){
		//Initialize the lookups
		$(component.find(element).getElement()).autocomplete({
			lookup : function(query, done){

				//Lightning won't execute the action without this mess of a timeout and callback function...thanks lightning
				window.setTimeout(
					$A.getCallback(function(){
						if(component.isValid()){
							helper.doCallout(component, "c.doSearch", {query : query}).then(function(results){
                                done({suggestions : results});
							});
						}
					}), 10
				);

			},
			beforeRender : function(container, suggestions){
				container.find('.autocomplete-suggestion').each(function(i, suggestion){
                    var icon = $('<span class="slds-icon_container slds-icon-standard-contact slds-m-right--small"><span><svg class="slds-icon"><img src="http://i.imgur.com/VgnxLrV.png" style="position:absolute;left:7px;top:37px;"/></svg></span><span class="slds-assistive-text" data-aura-rendered-by="38:70;a"></span></span>');
                    $(suggestion).prepend(icon[0]);
				});
			},
			onSelect : onSelect,
			onInvalidateSelection : onInvalidate,
			minChars : 2,
			groupBy : 'category',
			showNoSuggestionNotice : true
		});
	},

	uploadChunk : function(component, fileName, fileType, fileContents, fromPos, toPos, attachId, parentId, callback){
		var that = this;
		var chunk = fileContents.substring(fromPos, toPos);

		window.setTimeout(
			$A.getCallback(function(){
				if(component.isValid()){
					that.doCallout(component, "c.saveTheChunk", {
						parentId : parentId,
						fileName : fileName,
						base64Data : encodeURIComponent(chunk),
						contentType : fileType,
						fileId : attachId
					}).then(function(returnId){
						fromPos = toPos;
						toPos = Math.min(fileContents.length, fromPos + that.CHUNK_SIZE);
						if(fromPos < toPos){
							that.uploadChunk(component, fileName, fileType, fileContents, fromPos, toPos, returnId, parentId, callback);
						}else{
							callback(parentId);
						}
					})
				}
			}), 1
		);
	},

	readFile: function(component, helper, file, callback) {
		if (!file) return;
		var reader = new FileReader();
		reader.onloadend = function() {
			var fileContents = reader.result;
			var base64Mark = 'base64,';
			var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;

			callback({
				fileName : file.name,
				base64Data : fileContents.substring(dataStart),
				contentType : file.type,
				size : file.size
			});
		};
		reader.readAsDataURL(file);
	},
	showToast : function(title, message, type) {
	    var toastEvent = $A.get("e.force:showToast");
	    toastEvent.setParams({
	        "title": title,
	        "message": message,
					"type" : type
	    });
	    toastEvent.fire();
	},
	validate : function(component){
        console.log(component.get("v.model"));
		return (component.get("v.model").record.Subject && component.get("v.model").record.Subject.trim().length > 0
						&& component.get("v.model").record.ContactId && component.get("v.model").record.ContactId.trim().length > 0
						&& component.get("v.model").record.Priority && component.get("v.model").record.Priority.trim().length > 0
						&& component.get("v.model").record.BERT_Environment__c && component.get("v.model").record.BERT_Environment__c.trim().length > 0
						&& component.get("v.model").record.Canam_Product__c && component.get("v.model").record.Canam_Product__c.trim().length > 0);
	},
	showSpinner : function(cmp, event, helper){
		var spinner = cmp.find("mySpinner");
		$A.util.removeClass(spinner, "slds-hide");
	},
	hideSpinner : function(cmp, event, helper){
		var spinner = cmp.find("mySpinner");
		$A.util.addClass(spinner, "slds-hide");
	}
})