@isTest
public class TEST_JIRAUtilities {
    static testMethod void createJiraTest() {
        Case c = createCase();
        JIRAUtilities.createJIRACase(c.id);
        
        Case_Comment__c comment = new Case_Comment__c();
        comment.case__c = c.id;
        comment.public_comment__c = 'This is the body of the comment';        
        insert comment;
        jirautilities.sendCommentToJIRA(comment.id);
    }
    
    static testMethod void retrieveJIRAStatusTest(){
        Case c = createCase();
        JIRAUtilities.retrieveJIRAStatus(c.id);
    }
    
    static testMethod void testBatchJob(){
        Case c = createCase();
        c.Status = 'In Progress';
        c.JIRA_Id__c = '12345';
        JIRAStatus_Batch b = new JIRAStatus_Batch();
        database.executebatch(b,1);		 
    }
    
    static testMethod void sendAttachmentsTest() {
        Case c = createCase();
        JIRAUtilities.sendAttachments(c.id);
    }
    
    static Case createCase (){
        Account acc = new Account();
        acc.Name = 'test';
        acc.Account_Status__c = 'Target';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'test';
        con.AccountId = acc.id;
        con.email='test@test.com';
        con.Phone='5554441234';
        insert con;
        
        Case c = new Case();
        //Case jiraCase = [SELECT ID, JIRA_ID__C, JIRA_KEY__C, JIRA_Link__c, 
        //        JIRA_Dev_Team__c,JIRA_Priority__c, subject, reason, 
        //        What_is_your_Question__c, SuppliedName  
        //        from CASE where id = :caseId];
        c.subject = 'Test Case Case';
        c.reason ='Test Case Reason';
        c.JIRA_Link__c = 'http://www.test.com';
        c.description = 'Test Description';
        c.JIRA_Labels__c = 'label';
        c.Integrate_with_JIRA__c = true;
        c.ContactId = con.id;
        insert c;
        
        Case_Comment__c comment = new Case_Comment__c();
        comment.case__c = c.id;
        comment.public_comment__c = 'This is the body of the comment';        
        insert comment;

        Attachment attachment = new Attachment();   	
        attachment.parentId = c.id;
        attachment.Name = 'Unit Test Attachment';
    	Blob bodyBlob = Blob.valueOf('Unit Test Attachment Body');
    	attachment.body = bodyBlob;
        insert attachment;

        return c;        
    }
}