global class  JIRAStatus_Batch implements Database.Batchable<Sobject>, Database.AllowsCallouts{
    global Database.Querylocator start(Database.BatchableContext BC){
        return Database.getQuerylocator([SELECT Id, JIRA_Id__c, Status FROM Case where JIRA_ID__c != '' and Status != 'Closed']);
    }
    global void execute(Database.BatchableContext BC, List<Case> caseList){
        for(Case myCase : caseList){
            system.debug('Updating case id: ' + myCase.id);
            JIRAUtilities.retrieveJIRAStatus(myCase.id);
        }
    }
    
    global void finish(Database.BatchableContext BC){        
    }
}