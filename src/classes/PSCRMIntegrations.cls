global with sharing class PSCRMIntegrations {
    @future(callOut=true)
    public static void CreateInternalNote(String caseCommentId) {
        PSCRM_Settings__c PSCRMSettings;
        
        if(Test.isRunningTest()){
            PSCRMSettings = new PSCRM_Settings__c();
            PSCRMSettings.CreateInternalNote__c   = 'http://tempuri.org/ICRMNotesAndAttachments/CreateInternalNote';
            PSCRMSettings.CreateSRAttachment__c  = 'http://tempuri.org/ICRMNotesAndAttachments/CreateSRAttachment';
            PSCRMSettings.CreateSRNote__c  = 'http://tempuri.org/ICRMNotesAndAttachments/CreateSRNote';
            PSCRMSettings.Endpoint__c = 'http://cat-productsupportservices-qa.azurewebsites.net/CRMNotesAndAttachments.svc';
        }else{
            PSCRMSettings = PSCRM_Settings__c.getInstance('PSCRM');            
        }
        
        List<Case_Comment__c> caseComments = [Select External_System_ID__c, id, case__c, internal_comment__c, createdbyid From case_comment__c Where Id = :caseCommentId];
        
        if (caseComments.size() == 0){
            return;
        }
        Case_Comment__c caseComment = caseComments[0];
        List<Case> cases = [select SR_Number__c from case where id =: caseComment.case__c];
        Case caseView = cases[0];
        
        List<User> users = [select alias, email from user where id =: caseComment.createdbyid];
        User userView = users[0];

        String requestStr = '<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:cat="http://schemas.datacontract.org/2004/07/CAT.PSCRM.WCF">' +
            '  <x:Header/>' +
            '  <x:Body>' +
            '    <tem:CreateInternalNote>' +
            '       <tem:internalNote>';
        requestStr += '<cat:NoteText>' + caseComment.internal_comment__c + '</cat:NoteText>';
        requestStr += '<cat:SrNumberVal>' + caseView.SR_Number__c + '</cat:SrNumberVal>';
        requestStr += '<cat:SubmittedBy>' + userView.email + '</cat:SubmittedBy>';
        requestStr += '</tem:internalNote>';
        requestStr += '</tem:CreateInternalNote>';
        requestStr += '</x:Body></x:Envelope>';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(PSCRMSettings.Endpoint__c);
        req.setHeader('Content-Type','text/xml; charset=utf-8');
        req.setMethod('POST');
        req.setBody(requestStr);
        req.setHeader('SOAPAction',PSCRMSettings.CreateInternalNote__c);
        req.setTimeout(120000);
        system.debug('XML' + requestStr);
        system.debug(req.toString());
        HttpResponse res = null;
        if (Test.isRunningTest()){
            String strResponse = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
                'xmlns:per="http://platform.uptake.edu/services/uptake/person/PersonService">' +
                '<soapenv:Header>' +
                '<gateway:transactionId xmlns:gateway="http://platform.uptake.edu/gateway">' +
                '2868FEAA-CB14-1BA7-78B5-9DAB5FF54945' +
                '</gateway:transactionId>' +
                '</soapenv:Header>' +
                '<soapenv:Body>' +
                '<per:createUpdatePersonInfoResponse>' +
                '<per:profileId>' +
                '1234f567-bc23-42e8-924c-1234asdf5678' +
                '</per:profileId>' +
                '</per:createUpdatePersonInfoResponse>' +
                '</soapenv:Body>' +
                '</soapenv:Envelope>';
            
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody(strResponse);
        }
        else{
            res = h.send(req);
        }
        system.debug(res.getStatus());
        system.debug(res.getBody());
        
        String result = res.getBody();
        
        // put right after the request is made
        String noteResult = '';
        if (!String.isEmpty(res.getBody())) {
            // get the HTTP response as an XML stream
            XmlStreamReader reader = res.getXmlStreamReader();            
            // Read through the XML
            while(reader.hasNext()) {                
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                    if (reader.getLocalName().equals('CreateInternalNoteResult')) {
                        noteResult = readCharacters(reader);
                    }
                }
                reader.next();
            }            
        }
        if (String.isEmpty(noteResult)) {
			caseComment.External_System_ID__c = 'Failure.  Please contact admin';
        } else {
			caseComment.External_System_ID__c = noteResult;
        }
        update caseComment;
        return;
    }
    
    @future(callOut=true)
    public static void CreateSRNote(String caseCommentId) {
        PSCRM_Settings__c PSCRMSettings;
        if(Test.isRunningTest()){
            PSCRMSettings = new PSCRM_Settings__c();
            PSCRMSettings.CreateInternalNote__c   = 'http://tempuri.org/ICRMNotesAndAttachments/CreateInternalNote';
            PSCRMSettings.CreateSRAttachment__c  = 'http://tempuri.org/ICRMNotesAndAttachments/CreateSRAttachment';
            PSCRMSettings.CreateSRNote__c  = 'http://tempuri.org/ICRMNotesAndAttachments/CreateSRNote';
            PSCRMSettings.Endpoint__c = 'http://cat-productsupportservices-qa.azurewebsites.net/CRMNotesAndAttachments.svc';
        }else{
            PSCRMSettings = PSCRM_Settings__c.getInstance('PSCRM');            
        }
        
        List<Case_Comment__c> caseComments = [Select External_System_ID__c, id, case__c, public_comment__c, createdbyid From case_comment__c Where Id = :caseCommentId];
        
        if (caseComments.size() == 0){
            return;
        }
        Case_Comment__c caseComment = caseComments[0];
        List<Case> cases = [select SR_Number__c from case where id =: caseComment.case__c];
        Case caseView = cases[0];
        
        List<User> users = [select alias, email from user where id =: caseComment.createdbyid];
        User userView = users[0];

        String requestStr = '<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:cat="http://schemas.datacontract.org/2004/07/CAT.PSCRM.WCF">' +
            '  <x:Header/>' +
            '  <x:Body>' +
            '    <tem:CreateSRNote>' +
            '       <tem:srNote>';
        requestStr += '<cat:NoteText>' + caseComment.public_comment__c + '</cat:NoteText>';
        requestStr += '<cat:SrNoteContactCWSId>Uptake</cat:SrNoteContactCWSId>';
        requestStr += '<cat:SrNumberVal>' + caseView.SR_Number__c + '</cat:SrNumberVal>';
        requestStr += '<cat:SubmittedBy>' + userView.email + '</cat:SubmittedBy>';
        requestStr += '</tem:srNote>';
        requestStr += '</tem:CreateSRNote>';
        requestStr += '</x:Body></x:Envelope>';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(PSCRMSettings.Endpoint__c);
        req.setHeader('Content-Type','text/xml; charset=utf-8');
        req.setMethod('POST');
        req.setBody(requestStr);
        req.setHeader('SOAPAction',PSCRMSettings.CreateSRNote__c);
        req.setTimeout(120000);
        system.debug('XML' + requestStr);
        system.debug(req.toString());
        HttpResponse res = null;
        if (Test.isRunningTest()){
            String strResponse = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
                'xmlns:per="http://platform.uptake.edu/services/uptake/person/PersonService">' +
                '<soapenv:Header>' +
                '<gateway:transactionId xmlns:gateway="http://platform.uptake.edu/gateway">' +
                '2868FEAA-CB14-1BA7-78B5-9DAB5FF54945' +
                '</gateway:transactionId>' +
                '</soapenv:Header>' +
                '<soapenv:Body>' +
                '<per:createUpdatePersonInfoResponse>' +
                '<per:profileId>' +
                '1234f567-bc23-42e8-924c-1234asdf5678' +
                '</per:profileId>' +
                '</per:createUpdatePersonInfoResponse>' +
                '</soapenv:Body>' +
                '</soapenv:Envelope>';
            
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody(strResponse);
        }
        else{
            res = h.send(req);
        }
        system.debug(res.getStatus());
        system.debug(res.getBody());
        
        String result = res.getBody();
        
        // put right after the request is made
        String noteResult = '';
        if (!String.isEmpty(res.getBody())) {
            // get the HTTP response as an XML stream
            XmlStreamReader reader = res.getXmlStreamReader();            
            // Read through the XML
            while(reader.hasNext()) {                
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                    if (reader.getLocalName().equals('CreateSRNoteResult')) {
                        noteResult = readCharacters(reader);
                    }
                }
                reader.next();
            }            
        }
        if (String.isEmpty(noteResult)) {
			caseComment.External_System_ID__c = 'Failure.  Please contact admin';
        } else {
			caseComment.External_System_ID__c = noteResult;
        }
        update caseComment;
        return;
    }
    
    @future(callOut=true)
    public static void CreateSRAttachment(String attachmentId) {
        PSCRM_Settings__c PSCRMSettings;
        if(Test.isRunningTest()){
            PSCRMSettings = new PSCRM_Settings__c();
            PSCRMSettings.CreateInternalNote__c   = 'http://tempuri.org/ICRMNotesAndAttachments/CreateInternalNote';
            PSCRMSettings.CreateSRAttachment__c  = 'http://tempuri.org/ICRMNotesAndAttachments/CreateSRAttachment';
            PSCRMSettings.CreateSRNote__c  = 'http://tempuri.org/ICRMNotesAndAttachments/CreateSRNote';
            PSCRMSettings.Endpoint__c = 'http://cat-productsupportservices-qa.azurewebsites.net/CRMNotesAndAttachments.svc';
        }else{
            PSCRMSettings = PSCRM_Settings__c.getInstance('PSCRM');            
        }
        
        List<attachment> attachments = [Select parentid,name,bodylength,contenttype,body,description From attachment Where Id = :attachmentid];
        
        if (attachments.size() == 0){
            return;
        }
        attachment attachmentView = attachments[0];
        
        List<Case> cases = [select SR_Number__c from case where id =: attachmentView.parentid];
        Case caseView = cases[0];
        
		String fileString=EncodingUtil.Base64Encode(attachmentView.body);
        String requestStr = '<x:Envelope xmlns:x="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/" xmlns:cat="http://schemas.datacontract.org/2004/07/CAT.PSCRM.WCF">' +
            '  <x:Header/>' +
            '  <x:Body>' +
            '    <tem:CreateSRAttachment>' +
            '       <tem:srAttachment>';
        requestStr += '<cat:FileContentLength>' + attachmentView.bodylength + '</cat:FileContentLength>';
        requestStr += '<cat:FileContentTye>' + attachmentView.contenttype + '</cat:FileContentTye>';
        requestStr += '<cat:FileData>' + fileString + '</cat:FileData>';
        requestStr += '<cat:FileName>' + attachmentView.name + '</cat:FileName>';
        requestStr += '<cat:SrNumberVal>' + caseView.sr_number__c + '</cat:SrNumberVal>';
        requestStr += '</tem:srAttachment>';
        requestStr += '</tem:CreateSRAttachment>';
        requestStr += '</x:Body></x:Envelope>';
        
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(PSCRMSettings.Endpoint__c);
        req.setHeader('Content-Type','text/xml; charset=utf-8');
        req.setMethod('POST');
        req.setBody(requestStr);
        req.setHeader('SOAPAction',PSCRMSettings.CreateSRAttachment__c);
        req.setTimeout(120000);
        system.debug('XML' + requestStr);
        system.debug(req.toString());
        HttpResponse res = null;
        if (Test.isRunningTest()){
            String strResponse = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" ' +
                'xmlns:per="http://platform.uptake.edu/services/uptake/person/PersonService">' +
                '<soapenv:Header>' +
                '<gateway:transactionId xmlns:gateway="http://platform.uptake.edu/gateway">' +
                '2868FEAA-CB14-1BA7-78B5-9DAB5FF54945' +
                '</gateway:transactionId>' +
                '</soapenv:Header>' +
                '<soapenv:Body>' +
                '<per:createUpdatePersonInfoResponse>' +
                '<per:profileId>' +
                '1234f567-bc23-42e8-924c-1234asdf5678' +
                '</per:profileId>' +
                '</per:createUpdatePersonInfoResponse>' +
                '</soapenv:Body>' +
                '</soapenv:Envelope>';
            
            res = new HttpResponse();
            res.setStatusCode(200);
            res.setStatus('OK');
            res.setBody(strResponse);
        }
        else{
            res = h.send(req);
        }
        system.debug(res.getStatus());
        system.debug(res.getBody());
        
        String result = res.getBody();
        
        // put right after the request is made
        String noteResult = '';
        if (!String.isEmpty(res.getBody())) {
            // get the HTTP response as an XML stream
            XmlStreamReader reader = res.getXmlStreamReader();            
            // Read through the XML
            while(reader.hasNext()) {                
                if (reader.getEventType() == XmlTag.START_ELEMENT) {
                    if (reader.getLocalName().equals('CreateSRAttachmentResult')) {
                        noteResult = readCharacters(reader);
                    }
                }
                reader.next();
            }            
        }
        string currentDescription = '';
        if(attachmentView.description != null){
            currentDescription = attachmentView.description;
        }
        if (String.isEmpty(noteResult)) {
			attachmentView.description = currentDescription + '|Integration Result: Failure. Please contact admin';
        } else {
			attachmentView.description = currentDescription + '|Integration Result:' + noteResult;
        }
        update attachmentView;
        return;
    }
    
    public static String readCharacters(XMLStreamReader reader) {
        String result = '';
        while(reader.hasNext()) {
            if (reader.getEventType() == XmlTag.CHARACTERS || reader.getEventType() == XmlTag.CDATA) {
                result += reader.getText();
            } else if (reader.getEventType() == XmlTag.END_ELEMENT) {
                break;                
            }
            reader.next();
        }
        return result;                
    }
}