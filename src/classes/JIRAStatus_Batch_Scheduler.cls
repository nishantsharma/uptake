global class JIRAStatus_Batch_Scheduler implements Schedulable {
	public static string CRON_EXP_0 = '0 0 * * * ? ';
	public static string CRON_EXP_10 = '0 10 * * * ? ';
	public static string CRON_EXP_20 = '0 20 * * * ? ';
	public static string CRON_EXP_30 = '0 30 * * * ? ';
	public static string CRON_EXP_40 = '0 40 * * * ? ';
	public static string CRON_EXP_50 = '0 50 * * * ? ';
	global  void  execute(SchedulableContext  sc)  {
		JIRAStatus_Batch b = new JIRAStatus_Batch();
		database.executebatch(b,1);
	}
	
	 public static void start(){
        system.schedule('JIRA Status Update Job On Hour', CRON_EXP_0,new JIRAStatus_Batch_Scheduler() );
        system.schedule('JIRA Status Update Job 10', CRON_EXP_10,new JIRAStatus_Batch_Scheduler() );
        system.schedule('JIRA Status Update Job 20', CRON_EXP_20,new JIRAStatus_Batch_Scheduler() );
        system.schedule('JIRA Status Update Job 30', CRON_EXP_30,new JIRAStatus_Batch_Scheduler() );
        system.schedule('JIRA Status Update Job 40', CRON_EXP_40,new JIRAStatus_Batch_Scheduler() );
        system.schedule('JIRA Status Update Job 50', CRON_EXP_50,new JIRAStatus_Batch_Scheduler() );
    }


}