public with sharing class Create_IntegrationLog {
    public static final String Incoming_Service = 'Incoming';
    public static final String Outgoing_Service = 'Outgoing';
    public static void insertLogDetails(String systemName, String serviceType, String serviceName, String request, String response, String statusDetails, String logIdentifier){
        Integration_Log__c integrationLog = new Integration_Log__c();
        integrationLog.System__c = systemName;
        integrationLog.Service_Type__c = serviceType;
        integrationLog.Service_Name__c = serviceName;
        integrationLog.Request__c = request.abbreviate(15000);
        integrationLog.Response__c = response.abbreviate(15000);
        integrationLog.Status_Details__c = statusDetails.abbreviate(255);
        integrationLog.Log_Identifier__c = logIdentifier;
        insert integrationLog;
    }
}