public class AdditionalContactsTriggerHandler {
    
    public AdditionalContactsTriggerHandler(){}
    
    public void afterInsert(List<Additional_Contacts__c> lstAdditionalContacts) {
        
        updateCaseContacts(lstAdditionalContacts);
    }
    
    /*public void afterUpdate(List<Additional_Contacts__c> lstAdditionalContacts) {
        
        updateCaseContacts(lstAdditionalContacts);
    }*/
    
    public void afterDelete(List<Additional_Contacts__c> lstAdditionalContacts) {
        
        updateCaseContacts(lstAdditionalContacts);
    }
    
    private void updateCaseContacts(List<Additional_Contacts__c> lstAdditionalContacts){
     
        updateCaseContacts(lstAdditionalContacts, new Set<Id>());
    }
    
    private void updateCaseContacts(List<Additional_Contacts__c> lstAdditionalContacts, Set<Id> contactIdsToExclude){
        
        //check the parent of the updated/inserted/deleted record - caseId
        Map<String,Case> mapCaseIdToCase = new Map<String,Case>();
        
        for(Additional_Contacts__c objAdditionalContacts : lstAdditionalContacts){
            
            system.debug('objAdditionalContacts.Case__c *>'+objAdditionalContacts.Case__c);
            if(string.isNotBlank(objAdditionalContacts.Case__c)){
                    
                mapCaseIdToCase.put(objAdditionalContacts.Case__c, new Case(Id = objAdditionalContacts.Case__c, Additional_Contacts_Emails__c = ''));
            }
        }
        
        //get email field(Email__c) from additional contact related to parent and populate them semi-colon(;) separated into a long text area field created on case
        for(Additional_Contacts__c objAdditionalContacts : [SELECT Id, Case__c, Email__c 
                                                            FROM Additional_Contacts__c 
                                                            WHERE Case__c IN: mapCaseIdToCase.keySet() 
                                                            AND Contact__c != NULL
                                                            AND Contact__c NOT IN : contactIdsToExclude]){
            
            system.debug('objAdditionalContacts.Case__c 2 *>'+objAdditionalContacts);                                                    
            String strAdditionalContactsEmail = '';
            if(mapCaseIdToCase.containsKey(objAdditionalContacts.Case__c)){
                
                strAdditionalContactsEmail = mapCaseIdToCase.get(objAdditionalContacts.Case__c).Additional_Contacts_Emails__c;
                strAdditionalContactsEmail += (String.isNotBlank(strAdditionalContactsEmail)) ? ';'+objAdditionalContacts.Email__c : objAdditionalContacts.Email__c;
            }
            
            mapCaseIdToCase.put(objAdditionalContacts.Case__c, New Case(Id = objAdditionalContacts.Case__c, Additional_Contacts_Emails__c = strAdditionalContactsEmail));
            
        }
        
        system.debug('mapCaseIdToCase.values *>'+mapCaseIdToCase.values());
        //update the case record list
        Update mapCaseIdToCase.values();
    }
    
    //Call From ContactTrigger To Handle Updates on Contact Record To Additional_Contacts__c Record
    public void updateAdditionalEmailOnCase(List<Contact> lstContactNew, Map<Id,Contact> mapIdToContactOld){
        
        Set<String> setContactId = new Set<String>();
        for(Contact objContact : lstContactNew){
            
            if(objContact.Email != mapIdToContactOld.get(objContact.Id).Email){
                
                setContactId.add(objContact.Id);
            }
        }
        
        if(!setContactId.isEmpty()){
        
            updateCaseContacts([SELECT Id,Contact__c, Email__c,Case__c FROM Additional_Contacts__c WHERE Contact__c IN: setContactId]);
        }
        
    }
    
    public void updateAdditionalEmailOnCase(Map<Id,Contact> mapIdToContactOld){
        
        updateCaseContacts([SELECT Id,Contact__c, Email__c,Case__c FROM Additional_Contacts__c WHERE Contact__c IN: mapIdToContactOld.keySet()], mapIdToContactOld.keySet());
    }
}