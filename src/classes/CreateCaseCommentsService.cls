@RestResource(urlMapping='/createCaseComments/*')
global with sharing class CreateCaseCommentsService {
    global with sharing class ServiceResponseDTO {
        public String status {get; set;}
        public String message {get; set;}
        public String caseSalesforceID {get; set;}
        public String caseCommentSalesforceID {get; set;}
        public String caseCommentName {get; set;}
    }

    @HttpPost
    global static List<ServiceResponseDTO> createCaseComments(List<External_System_Comment__c> caseCommentsList) {		
        // create response object
        List<ServiceResponseDTO> response = new List<ServiceResponseDTO>();
        
        // default overall status details
        String statusDetails = '';

        try {
            // process the case comments
            if (!caseCommentsList.isEmpty()) {
                Database.SaveResult[] saveResults = Database.insert(caseCommentsList, false);
                // build response based on the results 
                Integer counter = 0;
                for (External_System_Comment__c caseComment : caseCommentsList) {                    
                    ServiceResponseDTO serviceResponseDTO = new ServiceResponseDTO();
                    serviceResponseDTO.caseSalesforceID = caseComment.Case__c;
                    serviceResponseDTO.caseCommentSalesforceID = caseComment.Id;
                    serviceResponseDTO.caseCommentName = caseComment.Name;
                    Database.SaveResult result = saveResults[counter++];
                    if (result.isSuccess()) {
	                    serviceResponseDTO.status = 'Success';  
	                    serviceResponseDTO.message = 'Case Comment created';
                    } else {
						Database.Error[] dbErrors = result.getErrors();
                        serviceResponseDTO.status = 'Error';
                        serviceResponseDTO.message = dbErrors[0].getMessage();
                    }
                    response.add(serviceResponseDTO);                        
                }
            }

	        // set overall status to success, payload processed where individual row
	        // success or error identified in the detail response
            statusDetails = 'Success';
	
        } catch (Exception e) {
            // log error
            System.debug('Error:'+e);
            
            // populate error response
            ServiceResponseDTO serviceResponseDTO = new ServiceResponseDTO();
            serviceResponseDTO.status = 'Error';
            serviceResponseDTO.message = e.getMessage();            
            response.add(serviceResponseDTO);

	        // set overall status to error, no payload processed
            statusDetails = 'Error';
        }

        // add to integration log
        Create_IntegrationLog.insertLogDetails('MSDynamics', Create_IntegrationLog.Incoming_Service, 'CreateCaseCommentsService', Json.serialize(caseCommentsList), Json.serialize(response), statusDetails, 'MSDynamics');

        return response;
    }
}