@isTest
private class AdditionalContactsTriggerHandlerTest {
    
    //Test_Util - created for test data creation - skeleton ready - put code
	private static testMethod void test() {

        //create account
        Account objAcc = Test_Util.createAccount('Test Account');
        insert objAcc;
        
        Account objAcc2 = Test_Util.createAccount('Test Account');
        insert objAcc2;
        
        //create contact1
        Contact objContact = Test_Util.createContact('Test contact2',objAcc.Id,'test@test.com');
        insert objContact;
        
        //create contact2
        Contact objContact2 = Test_Util.createContact('Test contact2',objAcc2.Id,'test2@test.com');
        insert objContact2;
        
        //create case
        Case objCase = Test_Util.createCase('Test Case');
        insert objCase;
        
        //create additional contact related to case
        Additional_Contacts__c objAdditionalContact = Test_Util.createAdditionalContacts(objContact.Id,objCase.Id);
        insert objAdditionalContact;
        insert Test_Util.createAdditionalContacts(objContact2.Id,objCase.Id);
        
        Additional_Contacts__c objAdditionalContact2 = Test_Util.createAdditionalContacts(objContact.Id,objCase.Id);
        insert objAdditionalContact2;
        insert Test_Util.createAdditionalContacts(objContact.Id,objCase.Id);
        
        system.assert(objCase.id != Null);
        
        Case objCase2 = [SELECT Id, Additional_Contacts_Emails__c FROM Case WHERE ID =: objCase.Id LIMIT 1];
        
        system.assertNotEquals(objCase.Additional_Contacts_Emails__c, objCase2.Additional_Contacts_Emails__c);
        
        system.assert(objCase2.Additional_Contacts_Emails__c != Null);
        
        //update contact2
        objContact2.email = 'test3@test.com';
        update objContact2;
        
        // update objAdditionalContact;
        
        //delete additionalcontact
        delete objAdditionalContact;
        
        //delete contact
        delete objContact;
	}
}