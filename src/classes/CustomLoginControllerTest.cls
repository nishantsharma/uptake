/*
Test Class for apex class - CustomLoginController
 */
@IsTest global with sharing class CustomLoginControllerTest {
    @IsTest(SeeAllData=true) global static void testCustomLoginController () {
        // Instantiate a new controller with all parameters in the page
        CustomLoginController controller = new  CustomLoginController ();
        controller.username = 'testuser@successvi.com';
        controller.password = '123456'; 
        
        controller.forwardToCustomAuthPage();                
        System.assertEquals(controller.login(),null);
                             
    }    
}