@isTest
public class Test_Util {

	public Static Account createAccount(String name){

        return new Account(Name = name);
	}
	
	public Static Contact createContact(String lName, String accountId,String email){

        return new Contact(lastName = lName,accountId = accountId,email=email);
	}
	
	public Static Case createCase(String subject){

        return new Case(Subject=subject);
	}
	
	public Static Additional_Contacts__c createAdditionalContacts(String contactId, String caseId){
	    
        return new Additional_Contacts__c(Case__c = caseId,Contact__c=contactId);
	}
}