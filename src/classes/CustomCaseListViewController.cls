/**
    * \arg ClassName        : CustomCaseListViewController
    * \arg CreatedOn        : 
    * \arg LastModifiedOn   : 
    * \arg CreatededBy      : Nikhil
    * \arg Description      : Apex class to fetch case records 
                              
**/
public with sharing class CustomCaseListViewController{
    
    public class caseWrapper{
        
      @AuraEnabled 
      public List<Case> lstCases {get;set;}
      @AuraEnabled
      public Map<String, List<String>> mapValues{get;set;}
       
       public caseWrapper(List<Case> lstCases, Map<String, List<String>> mapValues) {

            this.lstCases = lstCases;
            this.mapValues = mapValues;
            
        }
      
    }

    
    /**
        @MethodName : fetchCaseDetails
        @Param      : 
        @Description: 
    **/
    @AuraEnabled
    public static caseWrapper fetchCaseDetails(){    
        
        map<String, List<String>> mapValues = new map<String, List<String>>();
        
        List<Case> lstCases = [SELECT Id, BERT_Environment__c,Environment__c,
                                      BERT_Request_for_Quote__c,CaseNumber,Contact.Name,
                                      CreatedDate,BERT_Ticket_Number__c,Canam_Product__c,
                                      LastModifiedDate,Reason,Status,Subject,Priority,Type 
                                  FROM Case WHERE Status != 'Closed'];
        
        // Fetching Picklist values from picklist options method                          
        mapValues.put('Status', getPicklistOptions('Status'));
        mapValues.put('Priority', getPicklistOptions('Priority')); 
        mapValues.put('BERT_Environment__c', getPicklistOptions('BERT_Environment__c')); 
        mapValues.put('Canam_Product__c', getPicklistOptions('Canam_Product__c')); 
        return new caseWrapper(lstCases, mapValues);
    }
   
   /**
        @MethodName : filterCaseData
        @Param      : statusFilters, priorityFilters, environmentFilters, productFilters, isCheckedFilters
        @Description: It is used to filter the fetched cases based on all case records
    **/
    @AuraEnabled
    public static caseWrapper filterCaseData(String statusFilters, String priorityFilters, String environmentFilters, String productFilters, String isCheckedFilters) {
        
        //Fetch the cases without applying filters
        String Query = 'SELECT Id, BERT_Environment__c,Environment__c, ' +
                        'BERT_Request_for_Quote__c,CaseNumber,Contact.Name, ' +
                        'CreatedDate,BERT_Ticket_Number__c,Canam_Product__c, ' +
                        'LastModifiedDate,Reason,Status,Subject,Priority,Type ' +
                        'FROM Case ';
        
        Query += prepareFilter(Query, statusFilters,false, 'Status');
        Query += prepareFilter(Query, priorityFilters,false, 'Priority');
        Query += prepareFilter(Query, environmentFilters,false, 'BERT_Environment__c');
        Query += prepareFilter(Query, productFilters,false, 'Canam_Product__c');
        if(String.isNotBlank(isCheckedFilters)){
            Query += prepareFilter(Query, '', Boolean.valueOf(isCheckedFilters), 'BERT_Request_for_Quote__c');
        }
        system.debug('====query here==='+Query);
        
        return new caseWrapper((List<Case>)Database.query(Query), new map<String, List<String>>());
    }
   
   /**
        @MethodName : prepareFilter
        @Param      : query, strFilterValue, isChecked, strFieldName
        @Description: It is used in the dynamic query to apply respective filters based on the filter values
    **/
    private static String prepareFilter(String query, String strFilterValue, Boolean isChecked, String strFieldName) {

        if(String.isBlank(strFilterValue) &&  !strFieldName.equalsIgnoreCase('BERT_Request_for_Quote__c'))
            return '';

        String strFilter = '(';
        for(String str : strFilterValue.split(';')) {
            
            strFilter += ',\'' + str + '\'';
        }
        strFilter = strFilter.replaceFirst(',', '');
        strFilter += ')';
        
        //Seperate Query if the field is checkbox
        if(strFieldName.equalsIgnoreCase('BERT_Request_for_Quote__c')){
            
            if(query.contains('WHERE')){
                
                return ' AND ' + strFieldName + ' = ' + isChecked;
            }
            else
                return ' WHERE ' + strFieldName + ' = ' + isChecked;
            
        }else{
            //Seperate Query if the field is other than checkbox
            if(query.contains('WHERE'))
                return ' AND ' + strFieldName + ' IN ' + strFilter;
            else
                return ' WHERE ' + strFieldName + ' IN ' + strFilter; 
        }
    }
   
   /**
        @MethodName : getPicklistOptions
        @Param      : strFieldName
        @Description: A common describe call to fetch all picklist values from Case Object
    **/
    public static List<String> getPicklistOptions(String strFieldName) {
        
        List<String> lstPicklistOptions = new List<String>();
        
        for(Schema.PicklistEntry objPicklistOption : Case.sObjectType.getDescribe().Fields.getmap().get(strFieldName).getDescribe().getPicklistValues()) {
            
            if(objPicklistOption.isActive()){
                
                lstPicklistOptions.add(objPicklistOption.getValue());
            }
            
        }
        return lstPicklistOptions;
    }
    
}