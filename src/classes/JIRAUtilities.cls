global with sharing class JIRAUtilities {
    
    class JIRAResponse{
        public String id {get; set;}
        public String key {get; set;}
        public String self {get; set;}
        public JIRAField fields {get; set;}
    }
    
    class JIRAField{
        public JIRAStatus status {get; set;}
        public JIRAAssignee assignee {get; set;}
        public JIRAComments comment {get; set;}
        public list<JIRAFixVersions> fixVersions {get;set;}
    }
    
    class JIRAStatus{
        public String name {get; set;}
    }
    
    class JIRAAssignee{
        public String name {get; set;}
    }
    
    class JIRAComments{
        public List<JIRAComment> comments {get; set;}
    }
    
    class JIRAFixVersions{
        //public list<JIRAFixVersion> fixVersion {get; set;}
        public string name {get; set;}
    }
    
    class JIRAComment{
        public JIRAAuthor author {get; set;}
        public String id {get; set;}
        public String self {get; set;}
        public String body {get; set;}
        
    }
    
    class JIRAAuthor{
        public String displayName {get; set;}
    }
    
    webservice static String createJIRACase(Id caseId){
        //Pull Case Information
        //Call JIRA to create bug
        //Store JIRA Id
        
        JIRA_Settings__c jiraSettings;
        
        if(Test.isRunningTest()){
            jiraSettings = new JIRA_Settings__c();
            jiraSettings.Create_Case_URL__c  = '';
            jiraSettings.Issue_Type__c = 'Bug';
            jiraSettings.Password__c = 'password';
            jiraSettings.Project_Code__c  = 'test';
            jiraSettings.User_Name__c = 'user';
        }else{
            jiraSettings = JIRA_Settings__c.getInstance('Uptake Atlassian');            
        }
        Case jiraCase = [SELECT ID, CaseNumber, Canam_Product__c,contactid, jira_project__c, JIRA_Labels__c,initiative__c, value_stream__c, jira_environment__c, JIRA_Issue_type__c, JIRA_ID__C, JIRA_KEY__C, JIRA_Link__c, JIRA_Dev_Team__c,JIRA_Priority__c, jira_component__c, subject, reason, What_is_your_Question__c, description, SuppliedName  from CASE where id = :caseId];
        
        Contact contactRecord = [select account.name, name from contact where id =: jiraCase.ContactId];
        
        String caseCommentString;
        
        List<Case_Comment__c> caseCommentlst = [select id, internal_comment__c from Case_Comment__c where case__c =: caseId order by createddate asc limit 1 ];
        if (caseCommentlst.size() > 0){
            caseCommentString = caseCommentlst[0].internal_comment__c;
        }
        
        if(jiraCase.JIRA_Key__c != null ){
            return 'JIRA Ticket Already Exists';
        }
        
        String jiraDescription ='';
        if(jiracase.Description != '' && jiracase.description != null){
            jiraDescription = jiracase.Description.escapejava();
        } else if(jiracase.What_is_your_Question__c != ''){
            jiraDescription = jiracase.What_is_your_Question__c;
        } else {
            jiraDescription = caseCommentString;
        }
        
        //Grab the JIRA Labels and create label string for integration
        String jiraLabelString = '';
        if(jiracase.JIRA_Labels__c != '' && jiracase.JIRA_Labels__c != null){
            String[] labelValues = jiracase.JIRA_Labels__c.split(',');
            jiraLabelString = '"labels":' + (json.serialize(labelValues)) + ',';
        }
        
        String username = jiraSettings.user_name__c+':'+jiraSettings.password__c;
        Blob uName = Blob.valueOf(username);
        String auth = EncodingUtil.base64Encode(uName);
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(jiraSettings.Create_Case_URL__c);
        req.setMethod('POST');
        String authorizationHeader = 'Basic "'+auth+'"';
        req.setHeader('Authorization', authorizationHeader);
        
        String issue;
        if(jiracase.jira_issue_type__c == 'Story'){
            issue = '{"fields":{"project":{"key":"'+jiraCase.jira_project__c+'"},"components":[{"name":"'+jiraCase.JIRA_component__c+'"}],"customfield_15400":{"value":"Not Applicable"},"customfield_10601":{"value":"'+jiraCase.JIRA_Dev_Team__c+'"},"customfield_10700":{"value":"'+jiraCase.value_stream__c+'"},"customfield_11409":{"value":"'+jiracase.initiative__c+'"},'+ jiraLabelString +'"priority":{"name":"'+jiraCase.JIRA_priority__c+'"},"summary":"'+jiraCase.subject+'","description":"'+jiraDescription+'","issuetype":{"name":"'+jiraCase.jira_issue_type__c+'"}}}';
        } else {
            if(jiraCase.jira_project__c == 'UP'){
                issue = '{"fields":{"project":{"key":"'+jiraCase.jira_project__c+'"},"components":[{"name":"'+jiraCase.JIRA_component__c+'"}],"environment":"'+jiraCase.JIRA_Environment__c +'","customfield_10601":{"value":"'+jiraCase.JIRA_Dev_Team__c+'"},'+ jiraLabelString +'"priority":{"name":"'+jiraCase.JIRA_priority__c+'"},"summary":"'+jiraCase.subject+'","description":"'+jiraDescription+'","issuetype":{"name":"'+jiraCase.jira_issue_type__c+'"}}}';
            } else if(jiraCase.jira_project__c == 'TOR'){
            	issue = '{"fields":{"project":{"key":"'+jiraCase.jira_project__c+'"},"environment":"'+jiraCase.JIRA_Environment__c +'","customfield_16300":"' + jiracase.CaseNumber  + '","customfield_15501":"' + contactRecord.account.name + '","customfield_15500":"' + contactRecord.name + '","customfield_15502":[{"value":"'+jiraCase.Canam_Product__c +'"}],"customfield_10601":{"value":"'+jiraCase.JIRA_Dev_Team__c+'"},'+ jiraLabelString +'"priority":{"name":"'+jiraCase.JIRA_priority__c+'"},"summary":"'+jiraCase.subject+'","description":"'+jiraDescription+'","issuetype":{"name":"'+jiraCase.jira_issue_type__c+'"}}}';
            }else {
                issue = '{"fields":{"project":{"key":"'+jiraCase.jira_project__c+'"},"environment":"'+jiraCase.JIRA_Environment__c +'","customfield_15501":"' + contactRecord.account.name + '","customfield_15500":"' + contactRecord.name + '","customfield_15502":[{"value":"'+jiraCase.Canam_Product__c +'"}],"customfield_10601":{"value":"'+jiraCase.JIRA_Dev_Team__c+'"},'+ jiraLabelString +'"priority":{"name":"'+jiraCase.JIRA_priority__c+'"},"summary":"'+jiraCase.subject+'","description":"'+jiraDescription+'","issuetype":{"name":"'+jiraCase.jira_issue_type__c+'"}}}';
            }
          
        }
        system.debug(issue);
        req.setBody(issue);
        req.setHeader('content-type', 'application/json');
        Http http = new Http();
        JIRAResponse response;
        if(!Test.isRunningTest()){
            HttpResponse res = http.send(req);
            system.debug('STEVE[res] ' +res.getBody());
            response = (JIRAResponse) JSON.deserialize(res.getBody(), JIRAResponse.class);
            system.debug('STEVE[res] ' +res.getBody());
        }else{
            response = new JIRAResponse();
            response.id = '';
            response.key='123';
            response.self = 'http://www.test.com';
        }

        //completion of JIRA ticket creation
        jiraCase.JIRA_Id__c = response.id;
        jiraCase.JIRA_Key__c = response.key;
        jiraCase.JIRA_Link__c = response.self;
        update jiraCase;
        return 'Ticket ' + response.key + ' has been created';
    }
    
    webservice static String retrieveJIRAStatus(Id caseId){
        
        JIRA_Settings__c jiraSettings;
        
        if(Test.isRunningTest()){
            jiraSettings = new JIRA_Settings__c();
            jiraSettings.Create_Case_URL__c  = '';
            jiraSettings.Issue_Type__c = 'Bug';
            jiraSettings.Password__c = 'password';
            jiraSettings.Project_Code__c  = 'test';
            jiraSettings.User_Name__c = 'user';
        }else{
            jiraSettings = JIRA_Settings__c.getInstance('Uptake Atlassian');            
        }
        
        Case jiraCase = [SELECT ID, JIRA_Assignee__c, JIRA_ID__C, JIRA_FIX_VERSION__C, JIRA_KEY__C, JIRA_Link__c, JIRA_Status__c, subject, reason from CASE where id = :caseId];
        String username = jiraSettings.user_name__c+':'+jiraSettings.password__c;
        Blob uName = Blob.valueOf(username);
        String auth = EncodingUtil.base64Encode(uName);
        
        HttpRequest req = new HttpRequest();
        
        string endpoint;
        //check for jira endpoint
        if (jiracase.jira_link__c == null){
            endpoint = 'https://uptake.atlassian.net/rest/api/2/issue/' + jiracase.JIRA_Key__c;
        } else {
            endpoint = jiracase.jira_link__c;
        }
        req.setEndpoint(endpoint);
        req.setMethod('GET');
        String authorizationHeader = 'Basic "'+auth+'"';
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('content-type', 'application/json');
        Http http = new Http();
        JIRAResponse response;
        if(!Test.isRunningTest()){
            HttpResponse res = http.send(req);
            system.debug('STEVE[res] ' +res.getBody());
            response = (JIRAResponse) JSON.deserialize(res.getBody(), JIRAResponse.class);
        }else{
            response = new JIRAResponse();
            response.id = '';
            response.key='123';
            response.self = 'http://www.test.com';
            
            JIRAField field = new JIRAField();
            JIRAStatus status = new JIRAStatus();
            status.name = 'DONE';
            field.status = status;
            JIRAComments myComments = new JIRAComments();
            //Comments
            List<JIRAComment> commentList = new List<JIRAComment>();
            JIRAComment comment = new JIRAComment();        
            JIRAAuthor author = new JIRAAuthor();           
            author.displayName='Synegen';
            comment.author = author;
            comment.id='123';
            comment.body='comment body';
            comment.self='http://www.test.com';
            commentList.add(comment);
            myComments.comments = commentList;
            field.comment = myComments;
            response.fields=field;
            
            
        }
        try{
            jiraCase.JIRA_Status__c = response.fields.status.name;
            for(JIRAFixVersions jfix : response.fields.fixversions){
                jiraCase.JIRA_FIX_VERSION__C = jfix.name;
            }
            jiraCase.JIRA_Assignee__c = response.fields.assignee.name;
        } catch (Exception e){
            return('Cannot retrieve JIRA ticket ID. Please check JIRA for ticket information.');
        }
        update jiraCase;
        
        //Lets see if there are new comments
        List< External_System_Comment__c > comments = [SELECT ID__c FROM External_System_Comment__c where case__c = :jiraCase.ID];
        Map<String, ID> commentMap = new Map<String, Id>();
        for(External_System_Comment__c myComment : comments){
            commentMap.put(myComment.id__c, myComment.id);
        }
        
        List<External_System_Comment__c> newComments = new List<External_System_Comment__c>();
        for(JIRAComment jComment : response.fields.comment.comments){
            if(!commentMap.containsKey(jComment.id)){
                External_System_Comment__c newComment = new External_System_Comment__c();
                newComment.Created_By__c  = jComment.author.displayName;
                newComment.Internal_Comment__c  = jComment.body;
                newComment.Comment_Link__c = jComment.self;
                newComment.id__c = jComment.id;
                newComment.Case__c = jiraCase.id;
                newComment.System__c = 'JIRA';
                newComments.add(newComment);
            }
        }
        insert newComments;
        
        return response.fields.status.name;
    }
    
    
    webservice static void sendAttachments(Id caseId) {	
        // get settings
        JIRA_Settings__c jiraSettings = null;
        if (Test.isRunningTest()) {
            jiraSettings = new JIRA_Settings__c();
            jiraSettings.Create_Case_URL__c  = '';
            jiraSettings.Issue_Type__c = 'Bug';
            jiraSettings.Password__c = 'password';
            jiraSettings.Project_Code__c  = 'test';
            jiraSettings.User_Name__c = 'user';
        } else {
            jiraSettings = JIRA_Settings__c.getInstance('Uptake Atlassian');            
        }
        
        // get case
        Case jiraCase = [SELECT Id, JIRA_KEY__C FROM Case WHERE Id = :caseId];
        
        // get attachments and send
        List<Attachment> attachmentList = [SELECT Id, Name, Body FROM Attachment WHERE ParentId = :caseId];
        if (attachmentList.size() <= 0) {
            System.debug('No attachments to send.');
            return;            
        } else {
            for (Attachment attachment : attachmentList) {
                System.debug('Sending attachment to JIRA:'+attachment);
                uploadFileToJIRA(attachment.Body, attachment.Name, jiraCase.JIRA_KEY__C, jiraSettings);                
            }
        }
    }
    
    @future(callOut=true)
    public static void sendCommentToJIRA(string caseCommentID){
        JIRA_Settings__c jiraSettings;
        
        if(Test.isRunningTest()){
            jiraSettings = new JIRA_Settings__c();
            jiraSettings.Create_Case_URL__c  = '';
            jiraSettings.Issue_Type__c = 'Bug';
            jiraSettings.Password__c = 'password';
            jiraSettings.Project_Code__c  = 'test';
            jiraSettings.User_Name__c = 'user';
        }else{
            jiraSettings = JIRA_Settings__c.getInstance('Uptake Atlassian');            
        }
        
        String username = jiraSettings.user_name__c+':'+jiraSettings.password__c;
        Blob uName = Blob.valueOf(username);
        String auth = EncodingUtil.base64Encode(uName);
        
        //send comments
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        String authorizationHeader = 'Basic "'+auth+'"';
        req.setHeader('Authorization', authorizationHeader);
        
        case_comment__c caseCommentList = [select jira_comment_id__c, case__c, public_comment__c,createddate from case_comment__c where id =: caseCommentID];
        Case jiraCase = [select integrate_with_jira__c, JIRA_Key__c from case where id =: caseCommentList.Case__c];
        if (jiraCase.integrate_with_jira__c && caseCommentList.Public_Comment__c != null){
            req.setEndpoint(jiraSettings.Create_Case_URL__c + '/' + jiraCase.JIRA_Key__c + '/comment');
            String issue;
            issue = '{"body":"' + caseCommentList.Public_Comment__c.escapejava() + ' (created:' + caseCommentList.CreatedDate + ')"}';
            
            system.debug(issue);
            req.setBody(issue);
            req.setHeader('content-type', 'application/json');
            Http http = new Http();
            JIRAResponse response;
            if(!Test.isRunningTest()){
                HttpResponse res = http.send(req);
                system.debug('STEVE[res] ' +res.getBody());
                response = (JIRAResponse) JSON.deserialize(res.getBody(), JIRAResponse.class);
                system.debug('STEVE[res] ' +res.getBody());
            }else{
                response = new JIRAResponse();
                response.id = '';
                response.key='123';
                response.self = 'http://www.test.com';
            }
            caseCommentList.jira_comment_id__c = response.id;
            update caseCommentList;
        }    
    }
    
    public static void uploadFileToJIRA(Blob file_body, String file_name, String jiraKey, JIRA_Settings__c jiraSettings) {
        // create the body of the request as a blob
        String boundary = '----------------------------741e90d31eff';
        String header = '--'+boundary+'\nContent-Disposition: form-data; name="file"; filename="'+file_name+'";\nContent-Type: application/octet-stream';
        String footer = '--'+boundary+'--';             
        String headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        while(headerEncoded.endsWith('=')) {
            header+=' ';
            headerEncoded = EncodingUtil.base64Encode(Blob.valueOf(header+'\r\n\r\n'));
        }
        String bodyEncoded = EncodingUtil.base64Encode(file_body);
        
        Blob bodyBlob = null;
        String last4Bytes = bodyEncoded.substring(bodyEncoded.length()-4,bodyEncoded.length());
        
        if (last4Bytes.endsWith('==')) {
            last4Bytes = last4Bytes.substring(0,2) + '0K';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);
        } else if(last4Bytes.endsWith('=')) {
            last4Bytes = last4Bytes.substring(0,3) + 'N';
            bodyEncoded = bodyEncoded.substring(0,bodyEncoded.length()-4) + last4Bytes;
            footer = '\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);              
        } else {
            footer = '\r\n' + footer;
            String footerEncoded = EncodingUtil.base64Encode(Blob.valueOf(footer));
            bodyBlob = EncodingUtil.base64Decode(headerEncoded+bodyEncoded+footerEncoded);  
        }
        
        // establish auth and endpoint
        String auth_header = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(jiraSettings.user_name__c + ':' + jiraSettings.password__c));
        String reqEndPoint = jiraSettings.Create_Case_URL__c + '/' + jiraKey + '/attachments';
        
        // send the multipart file to JIRA
        HttpRequest req = new HttpRequest();
        req.setHeader('Content-Type','multipart/form-data; boundary='+boundary);
        req.setHeader('Authorization', auth_header);        
        req.setHeader('X-Atlassian-Token','no-check');
        req.setMethod('POST');
        req.setEndpoint(reqEndPoint);
        req.setBodyAsBlob(bodyBlob);
        req.setTimeout(120000);
        
        if (Test.isRunningTest()) {
            System.debug('Test is running...simulate sending the file');
        } else {
            Http http = new Http();
            HTTPResponse res = http.send(req);            
        }
    }    
}