@isTest
private class CustomCaseListViewController_Test {

    
	private static testMethod void testWithStatusClosed() {
	    
	    List<Case> lstCases = new List<Case>();
        
        for(Integer i=0;i<100;i++) {
            
            Case objCases = new Case(Subject='TestAccount' + i,Priority = 'P1', Status='Closed', Vertical__c = 'Torronto' );
            lstCases.add(objCases);
        }
        insert lstCases;


        Test.startTest();
        CustomCaseListViewController.getPicklistOptions('Status');
        CustomCaseListViewController.fetchCaseDetails();
        system.assertEquals(100, lstCases.size());
        CustomCaseListViewController.filterCaseData('Status', 'P1', 'Production','Confirmed','None' );
        system.assertEquals('P1', lstCases[0].Priority);
        system.assertEquals('Closed', lstCases[0].Status);
        
        Test.stopTest();
	}
	
	private static testMethod void testWithStatusOpen() {
	    
	    List<Case> lstNewCases = new List<Case>();
        
        for(Integer i=0;i<100;i++) {
            
            Case objCases = new Case(Subject='TestAccount' + i,Priority = 'P2', Status='Open', Vertical__c = 'Torronto' );
            lstNewCases.add(objCases);
        }
        insert lstNewCases;


        Test.startTest();
        CustomCaseListViewController.getPicklistOptions('Status');
        CustomCaseListViewController.fetchCaseDetails();
        CustomCaseListViewController.filterCaseData('Status', 'P2', 'Production','Confirmed','None' );
        system.assertEquals('P2', lstNewCases[0].Priority);
        system.assertEquals('Open', lstNewCases[0].Status);
        Test.stopTest();
	}

}