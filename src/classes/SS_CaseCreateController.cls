public with sharing class SS_CaseCreateController {

	private static Set<String> hiddenFields = new Set<String>{'Staging'};
	private static String DEFAULT_PRIORITY = 'P4';

	@AuraEnabled
	public static Model initializeModel(){

		Model m = new Model();
		SS_CaseCreateController.loadPicklist(m, Case.Priority.getDescribe().getPicklistValues(), 'Priority');
		SS_CaseCreateController.loadPicklist(m, Case.Canam_Product__c.getDescribe().getPicklistValues(), 'Canam_Product__c');
		SS_CaseCreateController.loadPicklist(m, Case.Type.getDescribe().getPicklistValues(), 'Type');
		SS_CaseCreateController.loadPicklist(m, Case.BERT_Environment__c.getDescribe().getPicklistValues(), 'BERT_Environment__c');

		return m;
	}

	@AuraEnabled
	public static List<LookupData> doSearch(String query){
		Map<Id, sObject> contactMap = new Map<Id, sObject>([FIND :query + '*' IN ALL FIELDS RETURNING Contact(Id, Name)].get(0));

		List<LookupData> data = new List<LookupData>();
		for(NetworkMember c : [SELECT Member.ContactId, Member.Name FROM NetworkMember WHERE NetworkId = :Network.getNetworkId() AND Member.ContactId IN :contactMap.keySet()]){
			data.add(new LookupData(c.Member.Name, new Map<String, Object>{'Id' => c.Member.ContactId, 'category' => '"' + query + '" in Contacts'}));
		}

		return data;
	}

	@AuraEnabled
	public static Map<String, Object> saveCase(String modelData){
		Model model = (Model)JSON.deserialize(modelData, SS_CaseCreateController.Model.class);
		try{
			insert model.record;

			if(model.firstContact != null)
				insert new Additional_Contacts__c(Case__c = model.record.Id, Contact__c = model.firstContact);
			if(model.secondContact != null)
				insert new Additional_Contacts__c(Case__c = model.record.Id, Contact__c = model.secondContact);

			return new Map<String, Object>{'success' => true, 'record' => model.record.Id};

		}catch(Exception e){
			return new Map<String, Object>{'success' => false, 'message' => e.getMessage()};
		}
	}

	@AuraEnabled
	public static Id saveTheChunk(Id parentId, String fileName, String base64Data, String contentType, String fileId) {
			if (fileId == '') {
					fileId = saveTheFile(parentId, fileName, base64Data, contentType);
			} else {
					appendToFile(fileId, base64Data);
			}

			return Id.valueOf(fileId);
	}

	private static void appendToFile(Id fileId, String base64Data) {
			base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

			ContentVersion a = [
					SELECT versionData
					FROM ContentVersion
					WHERE Id = :fileId
			];

		String existingBody = EncodingUtil.base64Encode(a.versionData);
		a.versionData = EncodingUtil.base64Decode(existingBody + base64Data);
		update a;
	}

	private static String saveTheFile(Id parentId, String fileName, String base64Data, String contentType){
		List<ContentVersion> cvList = new List<ContentVersion>();
		if(fileName != null){
			base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
			//Upload chatter file
			cvList.add(new ContentVersion(
				versionData = EncodingUtil.base64Decode(base64Data),
				title = fileName,
				pathOnClient = '/' + fileName,
				IsMajorVersion = false
			));
		}
		insert cvList;

		Map<Id, ContentVersion> cvMap = new Map<Id, ContentVersion>(cvList);

		List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
		for(ContentVersion v : [SELECT ContentDocumentId FROM ContentVersion WHERE Id IN :cvMap.keySet()]){
			cdlList.add(new ContentDocumentLink(LinkedEntityId = parentId,	ContentDocumentId = v.ContentDocumentId, ShareType = 'I'));
		}
		insert cdlList;

		return cvList.get(0).Id;
	}

	private static void loadPicklist(Model m, List<Schema.PicklistEntry> picklistValues, String field){
		for(Schema.PicklistEntry ple : picklistValues){
			if(hiddenFields.contains(ple.getValue()))
				continue;

			if(m.optionMap.get(field) == null)
				m.optionMap.put(field, new List<String>{ple.getValue()});
			else
				m.optionMap.get(field).add(ple.getValue());
		}
	}

	public class Model{
		@AuraEnabled public Case record{get;set;}
		@AuraEnabled public Map<String, List<String>> optionMap{get;set;}
		@AuraEnabled public String defaultContact{get;set;}
		@AuraEnabled public String firstContact{get;set;}
		@AuraEnabled public String secondContact{get;set;}

		public Model(){
			this.optionMap = new Map<String, List<String>>();
			this.record = new Case(Priority = DEFAULT_PRIORITY);
			try{
				User u = [SELECT ContactId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
				Contact c = [SELECT Name FROM Contact WHERE Id = :u.ContactId LIMIT 1];
				this.defaultContact = c.Name;
				this.record.ContactId = c.Id;
			}catch(QueryException e){
				//suppress exception
			}
		}
	}

	public class LookupData{
		@AuraEnabled public String value{get{return (value == null) ? '' : value;}set;}
		@AuraEnabled public Map<String, Object> data{get{return (data == null) ? new Map<String, Object>() : data;}set;}

		public LookupData(String value, Map<String, Object> data){
			this.value = value;
			this.data = data;
		}
	}

}