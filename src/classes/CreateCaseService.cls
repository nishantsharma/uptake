@RestResource(urlMapping='/createCase/*')
global with sharing class CreateCaseService {
    global with sharing class ServiceResponseDTO {
        public String status {get; set;}
        public String message {get; set;}
        public String salesforceID {get; set;}
        public String caseNumber {get; set;}
        public String caseURL {get; set;}
        public String token {get; set;}
    }
    
    @HttpPost
    global static ServiceResponseDTO createCase(Case newCase) {		
        // create response object
        ServiceResponseDTO response = new ServiceResponseDTO();
        
        try {
            // create case
            insert newCase;
            
			// retrieve the case to pull out relevant fields such as autonumbers, etc... as needed
			Case createdCase = [SELECT Id, CaseNumber, Thread_ID__c FROM Case WHERE Id =: newCase.id];
            
            // populate successful response from the createdCase
            response.status = 'Success';
            response.message = 'Case Created';
            response.salesforceID = createdCase.id;
            response.caseNumber = createdCase.CaseNumber;
            //response.caseURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + createdCase.Id;
            response.caseURL = 'https://uptake.force.com/partner/' + createdCase.Id;
            response.token = createdCase.Thread_ID__c;
            
        } catch (Exception e) {
            // log error
            System.debug('Error:'+e);
            
            // populate error response
            response.status = 'Error';
            response.message = e.getMessage();
        }
        
        // add to integration log
        Create_IntegrationLog.insertLogDetails('MSDynamics', Create_IntegrationLog.Incoming_Service, 'CreateCaseService', Json.serialize(newCase), Json.serialize(response), response.status, 'MSDynamics');
        
        return response;
    }
}