@isTest
private class Test_CaseFunctionality {
    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'test';
        acc.Account_Status__c = 'Target';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'test';
        con.AccountId = acc.id;
        con.email='test@test.com';
        con.Phone='5554441234';
        insert con;
        
        Case c = new Case();
        c.subject = 'test';
        c.description ='test';
        c.SR_Number__c = '12345';
        insert c;
        
        c.SuppliedEmail = 'test@test.com';
        update c;
        
        Case_Comment__c cm = new Case_Comment__c();
        cm.public_comment__c = 'test comment body';
        cm.Internal_Comment__c = 'test comment body';
        cm.case__c = c.id;
        insert cm;
        
        Solution s = new Solution();
        s.solutionnote = 'test solution';
        s.SolutionName = 'test solution name';
        insert s;
        
        CaseSolution cs = new CaseSolution();
        cs.CaseId = c.id;
        cs.SolutionId = s.id;
        insert cs;
        
        c.SuppliedName = 'System Tech';
        c.SR_Number__c = '12345';
        c.Status = 'Closed';
        update c;
        
    }
    
    static testMethod void myUnitTestTHQ() {
        Account acc = new Account();
        acc.Name = 'test';
        acc.Account_Status__c = 'Target';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'test';
        con.AccountId = acc.id;
        con.email='test@test.com';
        con.Phone='5554441234';
        insert con;
        
        Case c = new Case();
        c.subject = 'test';
        c.description ='test';
        c.status='Open';
        insert c;
        
        c.SuppliedEmail = 'test@test.com';
        update c;
        
        Case_Comment__c cm = new Case_Comment__c();
        cm.public_comment__c = 'test comment body';
        cm.case__c = c.id;
        insert cm;
        
        Solution s = new Solution();
        s.solutionnote = 'test solution';
        s.SolutionName = 'test solution name';
        insert s;
        
        CaseSolution cs = new CaseSolution();
        cs.CaseId = c.id;
        cs.SolutionId = s.id;
        insert cs;
        
        c.THQ_Ticket_ID__c = '12345';
        c.Status = 'Closed';
        update c;
        
    }
    
    static testMethod void myUnitTest2() {
    	Account acc = new Account();
        acc.Name = 'test';
        acc.Account_Status__c = 'Target';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'test';
        con.AccountId = acc.id;
        con.email='test@test.com';
        con.Phone='5554441234';
        insert con;
        
        Case c = new Case();
        c.subject = 'test';
        c.description ='test';
        insert c;
        
        EmailMessage em = new EmailMessage();
        em.textbody = 'test\ntest\nOn Tuesday';
        em.ParentId = c.id;
        em.FromAddress ='systemtech@test.com';
        insert em;
    }
    
    static testMethod void myUnitTest3() {
    	Account acc = new Account();
        acc.Name = 'test';
        acc.Account_Status__c = 'Target';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'test';
        con.AccountId = acc.id;
        con.email='test@test.com';
        con.Phone='5554441234';
        insert con;
        
        Case c = new Case();
        c.subject = 'test';
        c.description ='test';
        c.SR_Number__c ='123';
        insert c;
        
        Case_Comment__c cm = new Case_Comment__c();
        cm.internal_comment__c = 'test comment body';
        cm.case__c = c.id;
        insert cm;
        
        Attachment a = new attachment();
        Blob b = Blob.valueOf('Test Data');
        a.Name = 'test';
        a.ParentId = c.id;
        a.body = b;
        insert a;
    }

    static testMethod void caseAndCommentsServiceTest() {
    	Account acc = new Account();
        acc.Name = 'test';
        acc.Account_Status__c = 'Target';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'test';
        con.LastName = 'test';
        con.AccountId = acc.id;
        con.email='test@test.com';
        con.Phone='5554441234';
        insert con;
        
        Case c = new Case();
        c.subject = 'test';
        c.description ='test';
        
		// success condition
        CreateCaseService.ServiceResponseDTO caseResponse = CreateCaseService.createCase(c);
        System.assertNotEquals(null, caseResponse.salesforceID);
        
		// error condition
        caseResponse = CreateCaseService.createCase(null);
        System.assertEquals(null, caseResponse.salesforceID);

        List<External_System_Comment__c> caseCommentsList = new List<External_System_Comment__c>();        
        External_System_Comment__c comment = new External_System_Comment__c();
        comment.Case__c = c.id;
        comment.Name = 'Test Case Comment';
        caseCommentsList.add(comment);
        
		// success condition
        List<CreateCaseCommentsService.ServiceResponseDTO> caseCommentsResponse = 
            CreateCaseCommentsService.createCaseComments(caseCommentsList);
        System.assertEquals(1, caseCommentsResponse.size());

		// error condition
        caseCommentsResponse = CreateCaseCommentsService.createCaseComments(null);
        System.assertEquals(1, caseCommentsResponse.size());

    }

}