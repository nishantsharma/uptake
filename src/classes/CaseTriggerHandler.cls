public without sharing class CaseTriggerHandler {
    public static void execute(Boolean isBefore, Boolean isAfter, Boolean isInsert,Boolean isUpdate, List<Case> newList, Map<Id, Case> oldMap){
        updateCaseContact(isAfter, isInsert, isUpdate, newList, oldMap);
        setCaseContact(isAfter, isInsert, isUpdate, newList, oldMap);
        sendResolution(isAfter, isInsert, isUpdate, newList, oldMap);
        sendResolutionTHQ(isAfter, isInsert, isUpdate, newList, oldMap);
        sendJIRAComment(isAfter, isInsert, isUpdate, newList, oldMap);
    }
   	
    public static void sendJIRAComment(Boolean isAfter, Boolean isInsert, Boolean isUpdate, List<Case> newLst, Map<Id,Case> oldMap){
        set<id> caseIDs = new Set<ID>();
        if(isupdate){
            for (Case c : newLst){
                Case oldCase = oldmap.get(c.id);
                if (c.JIRA_Key__c != null && c.JIRA_Key__c !='' && c.JIRA_Key__c != oldCase.JIRA_Key__c){
                    caseIDs.add(c.id);
                }
            }
        }
        system.debug('Synegen:' + caseIds);
        
        if (caseIds.size() > 0){
            list<case_comment__c> caseCommentList = [select id from case_comment__c where case__c in: caseIds and jira_comment_id__c = null order by createddate asc];
            if (caseCommentList.size() > 0){
                for (Case_Comment__c cc : caseCommentList){
                    system.debug('Synegen:' + cc.Id);
                    JiraUtilities.sendCommentToJIRA(cc.Id);
                }
            }
        }
    }
    
    //Method to update the contact information on the case
    public static void updateCaseContact(Boolean isAfter, Boolean isInsert, Boolean isUpdate, List<Case> newLst, Map<Id,Case> oldMap){
        //Create contact map
        Map<string,Contact> contactMap = new Map<string,Contact>();
        List<Contact> contacts = [select id, name, accountid, email from contact];
        for (Contact c : contacts){
            contactMap.put(c.email, c);
        }
        
        string caseEmail;
        Set<ID> caseContactEmails = new Set<ID>();
        set<id> caseIDs = new Set<ID>();
        
        if (isupdate){
            for (Case c : newLst){
                Case oldCase = oldMap.get(c.id);
                //caseContactEmails.add(c.SuppliedEmail);
                if(c.suppliedemail != oldCase.SuppliedEmail){
                    caseEmail = c.SuppliedEmail;
                    caseIDs.add(c.id);
                }
            }
        }
        
        if (caseids.size() > 0){
            List<Case> caseLst = [select id,contactid,accountid from case where id in : caseIds];
            for (Case clst : caseLst){
                Contact cn = contactMap.get(caseEmail);
                clst.Contactid = cn.id;
                clst.AccountId = cn.AccountId;
            }
            update caseLst;
        }
    }
    
    //Method to link the incoming case to the SFDC Contact
    public static void setCaseContact(Boolean isAfter, Boolean isInsert, Boolean isUpdate, List<Case> newLst, Map<Id,Case> oldMap){
        //Set Variables
        Map<string,string> contactMap = new Map<string,string>();
        set<id> caseIDs = new Set<ID>();
        Set<string> caseEmails = new Set<string>();
        
        for (Case c : newLst){
            if (isInsert && c.contactid == null && (c.SR_Number__c != '' && c.SR_Number__c != null)){
                caseEmails.add(c.Preferred_Email_PSCRM__c);
                caseIds.add(c.id);
            }
        }
		system.debug('casemeail:' + caseEmails);
        //Create Contact Map
        if (caseEmails.size() > 0){
            List<Contact> contacts = [select id, name, accountid, email from contact where email in: caseEmails];
            for (Contact c : contacts){
                contactMap.put(c.email, c.id);
            }
        }
        system.debug('casemeail:' + contactMap);
        if (caseids.size() > 0){
            List<Case> caseLst = [select id,contactid,accountid,Preferred_Email_PSCRM__c from case where id in : caseIds];
            for (Case clst : caseLst){
                if(contactMap.containsKey(clst.Preferred_Email_PSCRM__c)){
                    clst.ContactId = contactMap.get(clst.Preferred_Email_PSCRM__c);
                }
            }
            update caseLst;
        }
    }
    
    //Method to send the resolution email
    public static void sendResolution(Boolean isAfter, Boolean isInsert, Boolean isUpdate, List<Case> newLst, Map<Id,Case> oldMap){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        //Custom setting to determine if trigger should run
        Case_Resolutions__c settings;
        if(test.isRunningTest()){
            settings = new Case_Resolutions__c();
            settings.Resolution_Email__c  = 'test@test.com';
            settings.Activate__c = true;
        }else{
            settings = Case_Resolutions__c.getValues('CAT');
        }
        
        //If custom setting is not active, do not run trigger
    	if(!settings.Activate__c)
        return;

        Map<id,Case> caseInfo = new Map<id,Case>();
        
        if(isupdate){
            for (Case c : newLst){
                Case oldCase = oldMap.get(c.id);
                //caseContactEmails.add(c.SuppliedEmail);
                if((c.status != oldCase.status) && c.status == 'Closed' && c.SR_Number__c != null){
                    caseInfo.put(c.id, c);
                }
            }
        }
        
        if (caseInfo.size() > 0){
            for (string key : caseInfo.keySet()){
                Case currentCase = caseInfo.get(key);
                
                //Generate PDF content
                String FORM_HTML_START = '<HTML><BODY>';
                String FORM_HTML_END = '</BODY></HTML>';
                String pdfContent = '' + FORM_HTML_START;
                try
                {
                    //pdfContent = '' + FORM_HTML_START;
                    pdfContent = pdfContent + '<H2>' + currentCase.CaseNumber +' Case Comments</H2>';
                    
                    string caseCommentBody ='';
                    List<Case_Comment__c> caseCommentLst = [select id,Public_Comment__c, Internal_Comment__c, createddate from Case_Comment__c  where case__c =: key order by createddate asc];
                    
                    for (Case_Comment__c commentlst : caseCommentLst){
                        if(commentlst.Public_Comment__c != '' && commentlst.Public_Comment__c != null){
                            caseCommentBody = 'Public Comment:<br>' + commentlst.Public_Comment__c.escapeHtml4();
                        } else {
                            //Internal comments have been removed based on the request of CAT.  Code will be commented out, but kept for future reference.
                            //caseCommentBody = 'Internal Comment:<br>' + commentlst.Internal_Comment__c.escapeHtml4();
                        }
                        pdfContent = pdfContent + caseCommentBody + '<br><br>';
                    }
                    
                    pdfContent = pdfContent + FORM_HTML_END;
                }catch(Exception e)
                {
                    pdfContent = '' + FORM_HTML_START;
                    pdfContent = pdfContent + '<P>THERE WAS AN ERROR GENERATING PDF: ' + e.getMessage() + '</P>';
                    pdfContent = pdfContent + FORM_HTML_END;
                }
                
                //pdfContent = pdfContent.escapeHtml4();
                //attach the pdf to the case
                /* PDF creation removed
                try
                {
                    Attachment attachmentPDF = new Attachment();
                    attachmentPDF.parentId = currentCase.Id;
                    attachmentPDF.Name = currentCase.CaseNumber + '.pdf';
                    attachmentPDF.body = Blob.toPDF(pdfContent); //This creates the PDF content
                    insert attachmentPDF;
                    }catch(Exception e)
                {
                    currentcase.addError(e.getMessage());
                }
				*/
                
                //Get Case solution information
                string solutionBody = '';
                List<CaseSolution> caseSolutionLst = [select solutionid from casesolution where caseid =: key order by createddate desc limit 1];
                if (caseSolutionLst.size() > 0){
                	List<Solution> solutionLst = [select solutionnote from solution where id =: caseSolutionLst[0].solutionid];
                	solutionBody = solutionLst[0].solutionnote;
                }
                
                //Construct the email envelope
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> sendTo = new List<String>();
                sendTo.add(settings.Resolution_Email__c);
                
                //Grab CC Emails
                List<String> ccAddresses = new List<String>();
                string ccemail = settings.CC_Email_Address_es__c;
                if(ccemail != null){
                    list<string> emails = ccemail.split(';');
                    for (string e : emails){
                        ccAddresses.add(e);
                    } 
                }
                mail.setToAddresses(sendTo);
                if(ccaddresses.size() > 0){
                    mail.setccAddresses(ccAddresses);
                }
                mail.setReplyTo('support@uptake.com');
                mail.setSenderDisplayName('Uptake Support');
                mail.setSubject(currentCase.Subject);
                String body = solutionBody;
                mail.setHtmlBody(body);
                /* PDF attachment is no longer needed.
                try
                {
                    List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                    
                    //create email attachments
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName('Case Comments.pdf');
                    efa.setBody(Blob.toPDF(pdfContent));
                    fileAttachments.add(efa);
                    mail.setFileAttachments(fileAttachments);
                }catch(Exception e)
                {
                    currentcase.addError(e.getMessage());
                }
				*/
                //add completed email envelope to mail list
                mails.add(mail);
            }
            
            //end send of all emails
            if(mails.size() > 0){
                Messaging.sendEmail(mails);
            }
        }
    }
    
    //Method to send the resolution email
    public static void sendResolutionTHQ(Boolean isAfter, Boolean isInsert, Boolean isUpdate, List<Case> newLst, Map<Id,Case> oldMap){
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
        
        //Custom setting to determine if trigger should run
        Case_Resolutions__c settings;
        if(test.isRunningTest()){
            settings = new Case_Resolutions__c();
            settings.Resolution_Email__c  = 'test@test.com';
            settings.Activate__c = true;
        }else{
            settings = Case_Resolutions__c.getValues('THQ');
        }
        
        //If custom setting is not active, do not run trigger
    	if(!settings.Activate__c)
        return;

        Map<id,Case> caseInfo = new Map<id,Case>();
        
        if(isupdate){
            for (Case c : newLst){
                Case oldCase = oldMap.get(c.id);
                //caseContactEmails.add(c.SuppliedEmail);
                if((c.status != oldCase.status) && c.status == 'Closed' && c.THQ_Ticket_ID__c  != null){
                    caseInfo.put(c.id, c);
                }
            }
        }
        
        if (caseInfo.size() > 0){
            for (string key : caseInfo.keySet()){
                Case currentCase = caseInfo.get(key);
                
                //Generate PDF content
                String FORM_HTML_START = '<HTML><BODY>';
                String FORM_HTML_END = '</BODY></HTML>';
                String pdfContent = '' + FORM_HTML_START;
                try
                {
                    //pdfContent = '' + FORM_HTML_START;
                    pdfContent = pdfContent + '<H2>' + currentCase.CaseNumber +' Case Comments</H2>';
                    
                    string caseCommentBody ='';
                    List<Case_Comment__c> caseCommentLst = [select id,Public_Comment__c, Internal_Comment__c, createddate from Case_Comment__c  where case__c =: key order by createddate asc];
                    
                    for (Case_Comment__c commentlst : caseCommentLst){
                        if(commentlst.Public_Comment__c != '' && commentlst.Public_Comment__c != null){
                            caseCommentBody = 'Public Comment:<br>' + commentlst.Public_Comment__c.escapeHtml4();
                        } else {
                            //Internal comments have been removed based on the request of CAT.  Code will be commented out, but kept for future reference.
                            //caseCommentBody = 'Internal Comment:<br>' + commentlst.Internal_Comment__c.escapeHtml4();
                        }
                        pdfContent = pdfContent + caseCommentBody + '<br><br>';
                    }
                    
                    pdfContent = pdfContent + FORM_HTML_END;
                }catch(Exception e)
                {
                    pdfContent = '' + FORM_HTML_START;
                    pdfContent = pdfContent + '<P>THERE WAS AN ERROR GENERATING PDF: ' + e.getMessage() + '</P>';
                    pdfContent = pdfContent + FORM_HTML_END;
                }
                
                //pdfContent = pdfContent.escapeHtml4();
                //attach the pdf to the case
                try
                {
                    Attachment attachmentPDF = new Attachment();
                    attachmentPDF.parentId = currentCase.Id;
                    attachmentPDF.Name = currentCase.CaseNumber + '.pdf';
                    attachmentPDF.body = Blob.toPDF(pdfContent); //This creates the PDF content
                    insert attachmentPDF;
                    }catch(Exception e)
                {
                    currentcase.addError(e.getMessage());
                }
                
                string caseResolutionSubject = 'THQ Ticket #' + currentCase.THQ_Ticket_ID__c + ' - ' + currentCase.Subject;
                
                //Get Case solution information
                string solutionBody = '';
                List<CaseSolution> caseSolutionLst = [select solutionid from casesolution where caseid =: key order by createddate desc limit 1];
                if (caseSolutionLst.size() > 0){
                	List<Solution> solutionLst = [select solutionnote from solution where id =: caseSolutionLst[0].solutionid];
                	solutionBody = solutionLst[0].solutionnote;
                }

                string caseRolutionBody = '{"CaseUpdate": {"Status":"' + currentCase.Status + '","Resolution":"' + solutionBody + '","Closed Date":"' + currentCase.ClosedDate + '","SalesforceID":"' + currentCase.Id + '"}}';
                
                //Construct the email envelope
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                List<String> sendTo = new List<String>();
                sendTo.add(settings.Resolution_Email__c);
                mail.setToAddresses(sendTo);
                
                //Grab CC Emails
                List<String> ccAddresses = new List<String>();
                string ccemail = settings.CC_Email_Address_es__c;
                if(ccemail != null){
                    list<string> emails = ccemail.split(';');
                    for (string e : emails){
                        ccAddresses.add(e);
                    } 
                }
                if(ccaddresses.size() > 0){
                    mail.setccAddresses(ccAddresses);
                }
                mail.setReplyTo('support@uptake.com');
                mail.setSenderDisplayName('Uptake Support');
                mail.setSubject(caseResolutionSubject);
                String body = caseRolutionBody;
                mail.setHtmlBody(body);
                
                try
                {
                    List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                    
                    //create email attachments
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName('Case Comments.pdf');
                    efa.setBody(Blob.toPDF(pdfContent));
                    fileAttachments.add(efa);
                    mail.setFileAttachments(fileAttachments);
                }catch(Exception e)
                {
                    currentcase.addError(e.getMessage());
                }
				
                //add completed email envelope to mail list
                mails.add(mail);
            }
            
            //end send of all emails
            if(mails.size() > 0){
                Messaging.sendEmail(mails);
            }
        }
    }
}