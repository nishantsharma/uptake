trigger CaseTrigger on Case (after update,after insert) {
    //Custom setting to determine if trigger should run
    Trigger_Handler__c settings;
    if(test.isRunningTest()){
        settings = new Trigger_Handler__c();
        settings.Activate__c = true;
    }else{
        settings = Trigger_Handler__c.getValues('Case');
    }

    //If custom setting is not active, do not run trigger
    if(!settings.Activate__c)
        return;
    if(checkRecursive.runOnce())
    {
        CaseTriggerHandler.execute(trigger.isbefore, trigger.isAfter, trigger.isInsert, trigger.isUpdate, trigger.new, trigger.oldMap);
    }
}