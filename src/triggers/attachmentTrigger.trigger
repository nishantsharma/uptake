trigger attachmentTrigger on Attachment (after insert) {
    Trigger_Handler__c settings;
    if(test.isRunningTest()){
        settings = new Trigger_Handler__c();
        settings.Activate__c = true;
    }else{
        settings = Trigger_Handler__c.getValues('PSCRM Attachment');
    }
    
    //If custom setting is not active, do not run trigger
    if(!settings.Activate__c)
        return;
    
    List<User> users = [select id,alias from user where name = 'Integration User'];
    if (users.size() > 0){
        User userView = users[0];
        
        if(Trigger.isInsert){
            for (Attachment att : Trigger.new){
                List<Case> cases = [select id, sr_number__c from case where id =: att.parentid];
                if(cases.size() > 0){
                    Case caseview = cases[0];
                    
                    if(att.createdbyid != userView.id && caseview.SR_Number__c != '' && caseview.SR_Number__c != null){
                        PSCRMIntegrations.CreateSRAttachment(att.id);
                    }
                }
            }
        }
    }
}