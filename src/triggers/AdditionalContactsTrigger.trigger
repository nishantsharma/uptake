trigger AdditionalContactsTrigger on Additional_Contacts__c (after insert,after update,after delete) {
    
    
    if(Trigger.isAfter){
        
        AdditionalContactsTriggerHandler objAdditionalContactsTriggerHandler = new AdditionalContactsTriggerHandler(); 
        
        if(Trigger.isInsert){
            system.debug('--Insert--->> *');
            objAdditionalContactsTriggerHandler.afterInsert(Trigger.new);
        /*} else if(Trigger.isUpdate){
            system.debug('--update--->> 2*');
            objAdditionalContactsTriggerHandler.afterUpdate(Trigger.new);*/
        } else if(Trigger.isDelete){
            system.debug('--delete--->> *');
            objAdditionalContactsTriggerHandler.afterDelete(Trigger.old);
        }
    }
}