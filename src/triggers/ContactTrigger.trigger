trigger ContactTrigger on Contact (after update, before delete) {
    
    if(Trigger.isAfter){
        
        if(Trigger.isUpdate){
            
            AdditionalContactsTriggerHandler objAdditionalContactsTriggerHandler = new AdditionalContactsTriggerHandler();
            objAdditionalContactsTriggerHandler.updateAdditionalEmailOnCase(Trigger.new,Trigger.oldMap);
        }
    } else {
        
        if(Trigger.isDelete){
            
            AdditionalContactsTriggerHandler objAdditionalContactsTriggerHandler = new AdditionalContactsTriggerHandler();    
            objAdditionalContactsTriggerHandler.updateAdditionalEmailOnCase(Trigger.oldMap);
        }
    }
}