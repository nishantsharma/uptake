trigger CreateCaseComment on EmailMessage (after insert) {
    Trigger_Handler__c settings;
    if(test.isRunningTest()){
        settings = new Trigger_Handler__c();
        settings.Activate__c = true;
    }else{
        settings = Trigger_Handler__c.getValues('Case Comment');
    }
    
    //If custom setting is not active, do not run trigger
    if(!settings.Activate__c)
        return;
    
    for (emailmessage emsg : trigger.new){
        string commentbody = emsg.textbody;
        string caseFinal = '';
        
        if (commentbody != '' && commentbody != null){
            /* Removed - 12/2: Code originally used to parse email-to-case thread prior to creating comment
            list <string> splitComment = commentbody.split('\n');
            system.debug(splitComment);
            
            for (string s : splitComment){
                if (!s.startsWith('On')){
                    if(caseFinal == ''){
                        caseFinal = caseFinal + s;
                    } else {
                        caseFinal = caseFinal + '\n' + s;
                    }
                } else {
                    break;
                }
            }
            system.debug(caseFinal);
			*/
            caseFinal = commentbody;
        }
        
        Case_Comment__c  cm = new Case_Comment__c ();
        if (emsg.fromaddress.contains('systemtech')){
            cm.Internal_Comment__c  = caseFinal;
        } else {
            cm.Public_Comment__c = caseFinal;
        }
        cm.Do_Not_Send_to_Customer__c = true;
        cm.Case__c  = emsg.parentid;
        system.debug (emsg.parentid);
        if(cm.Case__c != null){
            insert cm;
        }
        
        List<Case> caseInfo = [select id, first_response__c from case where id =: emsg.ParentId];
        if(caseinfo.size() > 0){
            boolean caseUpdate = false;
            for(Case c : caseInfo){
                if (c.first_response__c == null && !emsg.incoming && emsg.fromaddress == 'support@uptake.com'){
                    c.first_response__c = datetime.now();
                    caseUpdate = true;
                }
            }
            
            if(caseUpdate){
                update caseInfo;
            }
        }
    }
}