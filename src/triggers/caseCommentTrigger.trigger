trigger caseCommentTrigger on Case_Comment__c (after insert, after update) {
	Trigger_Handler__c settings;
    if(test.isRunningTest()){
        settings = new Trigger_Handler__c();
        settings.Activate__c = true;
    }else{
        settings = Trigger_Handler__c.getValues('PSCRM Case Comment');
    }
    
    //If custom setting is not active, do not run trigger
    if(!settings.Activate__c)
        return;

    if(Trigger.isInsert){
        for (Case_Comment__c ccc : Trigger.new){
            system.debug('internal' + ccc.Internal_Comment__c);
            system.debug('external' + ccc.public_comment__c);
            if(ccc.Internal_Comment__c != '' && ccc.Internal_Comment__c != null && ccc.case__r.sr_number__c != ''){
                PSCRMIntegrations.CreateInternalNote(ccc.id);
            }
            if(ccc.public_comment__c != '' && ccc.Public_Comment__c != null && ccc.case__r.sr_number__c != ''){
                PSCRMIntegrations.CreateSRNote(ccc.Id);
            }
        }
    }
}